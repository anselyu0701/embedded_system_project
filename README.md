# Guo-Dong's Tamagotchi

## About the Project

- Guo-Dong's Tamagotchi is a vedio game
- This game is the final project of embedded system design
- You can see more details in FinalProject_B10802217.pdf
- I sincerely hope you have a great time!

## Built with

- STM32F746G-Discovery
- KeilC
- STemWin
- BmpCvST

## Main Rules
- Guo-Dong is our good friend who need to be taken care of, so "health" and "fullness" are very important things for him.
- Guo-Dong reduces health by 1 point every 20 seconds. Every 10 seconds, it decreases the saturation level by 1 point. Every 60 seconds, it grows 1 year. Please keep an eye on Guo-Dong's growth and take good care of him.

